public abstract class Figure implements Areable
{
	String color;
	boolean fill;
	
	public Figure()
	{
		this("Red", false);
	}
	public Figure(String c, boolean f)
	{
		color = c;
		fill = f;
	}

}
