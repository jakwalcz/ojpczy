public class Sphere extends Circle implements Volumable
{
	private double radius;
	
	public Sphere()
	{
		this(2.5);
	}

	public Sphere (double r)
	{
		radius = r;
	}
	
	public double solveArea()
	{
		return 4*Math.PI*radius*radius;
	}
	
	public double solveVolume()
	{
		return (4/3)*Math.PI*Math.pow(radius, 3);
	}

}
