public class Circle extends Figure
{
	private double radius;
	
	public Circle()
	{
		this(1.0);
	}
	public Circle (double r)
	{
		this(r, "Red", true);
	}
	
	public Circle (double r, String c, boolean f)
	{
		super (c, f);
		radius = r;
	}
	
	public double solveArea()
	{
		return Math.PI*radius*radius;
	}
	
	public double solveCircuit()
	{
		return 2*Math.PI*radius;
	}

}
