public interface Areable 
{
	public abstract double solveArea();
	public abstract double solveCircuit();
}
