public class Rectangle extends Figure
{
	private double sideA;
	private double sideB;
	
	public Rectangle()
	{
		this(1.0, 2.0);
	}
	
	public Rectangle (double a, double b)
	{
		this(a, b, "Black", false);
	}
	
	public Rectangle (double a, double b, String c, boolean f)
	{
		super (c, f);
		sideA = a;
		sideB = b;
	}
	
	public double solveArea()
	{
		return sideA*sideB;
	}
	
	public double solveCircuit()
	{
		return 2*(sideA + sideB);
	}
	
}
