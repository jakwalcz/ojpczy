public interface Volumable 
{
	public abstract double solveVolume();
}
