import java.util.ArrayList;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;

public class Figures 
{
	private static Scanner sc;
//	I implement Scanner here, so I don't have to close Scanner at the end of code.

	public static void main(String[] args)
	{
		boolean isNotCorrect = true;
		sc = new Scanner(System.in);
		
		do
		{
			try 
			{
				System.out.println("Please enter your name: ");
				String name = sc.nextLine();
				
	//		I use set to check who run my program. If it is examiner program say hello.
				Set<String> zbior = new HashSet<String>();
				zbior.add("Julien");
				zbior.add("Jan");
				zbior.add("�ukasz");
				boolean bool = zbior.contains(name);
				if (bool == true)
				{
					System.out.println("Good morning profesor. ");
				}
				
				
				System.out.println("Enter radius of circle: ");
				double r = sc.nextDouble();
				
				System.out.println("Enter two sides of rectangle: ");
				double a = sc.nextDouble();
				double b = sc.nextDouble();
				
				System.out.println("Enter radius of circle: ");
				double s = sc.nextDouble();
				
				
				Circle circle = new Circle(r);
				Rectangle rect = new Rectangle(a, b);
				Sphere sphere = new Sphere(s);
				
//			Now I create new class, which can take form circle, rectangle and sphere.
				Figure[] figures = new Figure[3];
				figures[0] = circle;
				figures[1] = rect;
				figures[2] = sphere;	
				
	//		I use ArrayList to store a result. Next I will print it with color and fill. 
	//		I cannot put color and fill inside ArrayList cause there aren't a double variable.
				ArrayList<Double> resultc = new ArrayList<Double>();
				resultc.add(figures[0].solveArea());
				resultc.add(figures[0].solveCircuit());
//			During calling method java virtual machine choose proper method to the subclass.
				
				ArrayList<Double> resultr = new ArrayList<Double>();
				resultr.add(figures[1].solveArea());
				resultr.add(figures[1].solveCircuit());
				
				ArrayList<Double> results = new ArrayList<Double>();
				results.add(figures[2].solveArea());
				results.add(((Sphere) figures[2]).solveVolume());
				
				System.out.println("Area and circuit of circle for this radius: " + resultc + ", Color: " + figures[0].color + ", Fill: " + figures[0].fill);
				System.out.println("Area and circuit of recyangle for this two sides: " + resultr + ", Color: " + figures[1].color + ", Fill: " + figures[1].fill);
				System.out.println("Area and volume of sphere for this radius: " + results + " ");
				isNotCorrect = false;
			} 
			catch (InputMismatchException ae) 
			{
				System.err.println("Exception! Wrong variable, radius and sides have to be a double variable.");
				sc.nextLine();
				System.out.println(" ");
			}
		}
		while(isNotCorrect);

		
	}

}
